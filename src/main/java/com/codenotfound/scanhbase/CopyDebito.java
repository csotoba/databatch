package com.codenotfound.scanhbase;
import java.util.*;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static java.lang.Math.abs;

import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;


import org.apache.hadoop.hbase.filter. Filter;
import org.apache.hadoop.hbase.filter.SubstringComparator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopyDebito {
    public static void main(String args[]) throws IOException {
// Configuración para conexión a DataLake
        Map<String, String> mapaCampos = generaMapaCampos();

        Configuration config = HBaseConfiguration.create();
        //config.set("hbase.zookeeper.quorum", "10.248.22.9,10.248.22.7,10.248.22.8");
        config.set("hbase.zookeeper.quorum", "10.248.16.5,10.248.16.6,10.248.16.8");
        config.set("hbase.zookeeper.property.clientPort", "2181");
        config.set("hadoop.security.authentication", "kerberos");
        config.set("hbase.rpc.timeout", Integer.toString(60000));
        config.set("hbase.rpc.protection", "privacy");
        config.set("hbase.security.authentication", "kerberos");
        config.set("hbase.cluster.distributed", "true");
        config.set("hbase.master.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.master.keytab.file", "csotoba.keytab");
        config.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.regionserver.keytab.file", "csotoba.keytab");
        config.set("hbase.client.retries.number", Integer.toString(1));
        config.set("zookeeper.session.timeout", Integer.toString(60000));
        config.set("zookeeper.recovery.retry", Integer.toString(10));
        UserGroupInformation.setConfiguration(config);

        System.out.println("cargando rubros");
        Map<String,RubrosComercios> rubros =  readRubrosFromCSV("/home/csotoba/rubros_comercios_comuna.csv");
        System.out.println("listo");

// Instanciar la Tabla

        Connection conn = ConnectionFactory.createConnection(config);
        Table table_JNL = conn.getTable(TableName.valueOf("TDM_CCTOT001:TBJNL01"));
        Table table_JRB = conn.getTable(TableName.valueOf("TDM_CCTOT001:TBJRB01"));
        //Table table_cnv = conn.getTable(TableName.valueOf("syb_dbo:cn"));
        //Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:prueba_csoto"));
        //Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_cert"));
        Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_dev"));

       // "10003262","50003262","10003431","50003431","10003433","50003433","10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660"
//        List<String> cuentas = new ArrayList<String>(Arrays.asList("10003262","50003262","10003431","50003431","10003433","50003433","10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660"));
        //List<String> cuentas = new ArrayList<String>(Arrays.asList("10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660","13058178", "13058180", "90001792", "50009792", "10005603", "50005603", "49000082"));
        List<String> cuentas = new ArrayList<String>(Arrays.asList("13068521", "13068524", "10002908","50002908","10000830"));


        Long t1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-21 14:20:00", new ParsePosition(0)).getTime();
        Long t2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-21 14:30:00", new ParsePosition(0)).getTime();
        for(String cta_it : cuentas) {


            String cta_des = "0000" + cta_it; //13058178";
            cta_des = DigestUtils.sha1Hex(cta_des).toUpperCase();

// Instanciar el Scan con filtros correspondientes:
    /*
    String cta_ori = "000052203824";
    cta_ori = DigestUtils.sha256Hex(cta_ori).toUpperCase();
*/
            Scan scan = new Scan();
            t1 = t1 - 600000;
            t2 = t2 - 600000;
            scan.setTimeRange(t1, t2);
            Filter mnmFilter = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("JNL_MNM"), CompareOp.EQUAL, new SubstringComparator("68"));
            scan.setFilter(mnmFilter);


            ResultScanner scanner = table_JNL.getScanner(scan);

            int N = 10;
            int counter = 0;

            for (Result result = scanner.next(); result != null && counter < N; result = scanner.next()) {
                byte[] syskey = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("SYSKEY"));
                long mod = 9223372036854775807L - Long.parseLong(Bytes.toString(syskey));
                String row_des = cta_des + "|" + mod;
                System.out.println(row_des);
                Put put = new Put(Bytes.toBytes(row_des));

                // copio JNL TAL Y COMO ESTÁ
                NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap = result.getNoVersionMap();
                for (byte[] familyBytes : familyQualifierMap.keySet()) {
                    NavigableMap<byte[], byte[]> qualifierMap = familyQualifierMap.get(familyBytes);

                    for (byte[] qualifier : qualifierMap.keySet()) {
                        put.addColumn(familyBytes, qualifier, qualifierMap.get(qualifier));
                    }
                }


                //table_des.put(put);
                // ahora busco en la JRB

                Scan scan_jrb = new Scan();

                byte[] cuenta = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_COP"));
                byte[] monto = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_CPO_4"));
                byte[] fec_ctb = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_FEC_CTB"));
                byte[] fec_1 = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_FEC_PRC"));
                byte[] fec_2 = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_HOR_INI_PRC"));
                String fecha = Bytes.toString(fec_1).trim() + " " + Bytes.toString(fec_2).trim().substring(0, 5);
                // tsJNL = new SimpleDateFormat("yyyy-MM-dd HHmmss").parse(fecha, new ParsePosition(0)).getTime();

                //String tsJNL = formatoFecha.parse(registro.getJNLFECPRC.toString.trim + registro.getJNLHORINIPRC.toString.trim.dropRight(1)).getTime;
                t2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-21 19:00:00", new ParsePosition(0)).getTime();

                scan_jrb.setTimeRange(t1, t2);
                System.out.println("0000" + Bytes.toString(cuenta).substring(4) + "|" + Bytes.toString(fec_ctb) + "|" + Bytes.toString(monto));
                Filter filterJRB = new PrefixFilter(Bytes.toBytes("0000" + Bytes.toString(cuenta).substring(4) + "|" + Bytes.toString(fec_ctb) + "|" + Bytes.toString(monto)));

                scan_jrb.setFilter(filterJRB);
                ResultScanner scanner_jrb = table_JRB.getScanner(scan_jrb);
                int n1 = 0;
                int N1 = 1;
                for (Result result_jrb = scanner_jrb.next(); result_jrb != null && n1 < N1; result_jrb = scanner_jrb.next()) {

                    Put put_tra = new Put(Bytes.toBytes(row_des));

                    // copio mov TAL Y COMO ESTÁ
                    int p = 1;
                    NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap_mov = result_jrb.getNoVersionMap();
                    for (byte[] familyBytes : familyQualifierMap_mov.keySet()) {
                        NavigableMap<byte[], byte[]> qualifierMap_mov = familyQualifierMap_mov.get(familyBytes);

                        for (byte[] qualifier : qualifierMap_mov.keySet()) {
                            if (mapaCampos.containsKey(Bytes.toString(qualifier))) {
                                String column = mapaCampos.get(Bytes.toString(qualifier));
                                put_tra.addColumn(familyBytes, Bytes.toBytes(column), qualifierMap_mov.get(qualifier));
                            }
                        }
                    }


                    String codigo_rubro = Bytes.toString(result_jrb.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_USX")));
                    if (rubros.containsKey(codigo_rubro.substring(1, 9))) {
                        Put put_r = new Put(Bytes.toBytes(row_des));
                        RubrosComercios rubro = rubros.get(codigo_rubro.substring(1, 9));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r1"), Bytes.toBytes(rubro.getCodigo_comercio()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r2"), Bytes.toBytes(rubro.getCod_pv()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r3"), Bytes.toBytes(rubro.getRut()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r4"), Bytes.toBytes(rubro.getNombre_fantasia()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r5"), Bytes.toBytes(rubro.getGlosa_comercio()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r6"), Bytes.toBytes(rubro.getGlosa_rut()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r7"), Bytes.toBytes(rubro.getRubro_nivel1()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r8"), Bytes.toBytes(rubro.getRubro_nivel2()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r9"), Bytes.toBytes(rubro.getRubro_nivel3()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r10"), Bytes.toBytes(rubro.getId_rubro_nivel1()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r11"), Bytes.toBytes(rubro.getId_rubro_nivel2()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r12"), Bytes.toBytes(rubro.getId_rubro_nivel3()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r13"), Bytes.toBytes(rubro.getSubrubro_nivel2()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r14"), Bytes.toBytes(rubro.getLat()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r15"), Bytes.toBytes(rubro.getLon()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r16"), Bytes.toBytes(rubro.getInd_fuente()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r17"), Bytes.toBytes(rubro.getMinima_fecha()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r18"), Bytes.toBytes(rubro.getMaxima_fecha()));
                        put_r.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("r19"), Bytes.toBytes(rubro.getComuna()));
                        table_des.put(put_r);


                    }
                    // ahora la cnv
                    table_des.put(put);
                    table_des.put(put_tra);
                    Put put_n = new Put(Bytes.toBytes(row_des));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n1"), Bytes.toBytes("1116"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n2"), Bytes.toBytes("C"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n3"), Bytes.toBytes("Compra Tarjeta de Débito	"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n4"), Bytes.toBytes("855"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n5"), Bytes.toBytes("Rubrificar"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n6"), Bytes.toBytes("99"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n7"), Bytes.toBytes("Otros"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n8"), Bytes.toBytes("550"));
                    put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n9"), Bytes.toBytes("Otros"));
                    table_des.put(put_n);

                    n1++;
                    counter++;
                    System.out.println("regs: " + counter);
                }

            }
            scanner.close();

        }
//Cerrar la tabla
            table_JNL.close();
            table_JRB.close();
            table_des.close();

    }

    static Map<String,String> generaMapaCampos() {
        Map<String, String> mapCampos = new HashMap<String, String>();
        //TRANSFERENCIAS TRA
        mapCampos.put("JNL_NUM_CTA",                "b1");
        mapCampos.put("JNL_NUM_TML_ON2",            "b2");
        mapCampos.put("JNL_NUM_RCB",                "b3");
        mapCampos.put("JNL_FEC_CTB",                "b4");
        mapCampos.put("JNL_TIP_TRN",                "b5");
        mapCampos.put("JNL_COD_TRN",                "b6");
        mapCampos.put("JNL_SEC_TRN",                "b7");
        mapCampos.put("JNL_TIP_TML",                "b8");
        mapCampos.put("JNL_IFI_ORN",                "b9");
        mapCampos.put("JNL_SEC_MSG",                "b10");
        mapCampos.put("JNL_SNO_COD_TRN",            "b11");
        mapCampos.put("JNL_FEC_TRN",                "b12");
        mapCampos.put("JNL_HOR_TRN",                "b13");
        mapCampos.put("JNL_NUM_ABA",                "b14");
        mapCampos.put("JNL_NUM_BCH",                "b15");
        mapCampos.put("JNL_USX",                    "b16");
        mapCampos.put("JNL_IDN_TRN_FZD",            "b17");
        mapCampos.put("JNL_IND_RVS",                "b18");
        mapCampos.put("JNL_NUM_TJT",                "b19");
        mapCampos.put("JNL_VAL_TRN",                "b20");
        mapCampos.put("JNL_SNO_VAL_TRN",            "b21");
        mapCampos.put("JNL_IFI_HOS",                "b22");
        mapCampos.put("JNL_FEC_CTB_HOS",            "b23");
        mapCampos.put("JNL_TIP_AUT",                "b24");
        mapCampos.put("JNL_MTD_AUT",                "b25");
        mapCampos.put("JNL_COD_RTO_AUT",            "b26");
        mapCampos.put("JNL_TRK_2",                  "b27");
        mapCampos.put("JNL_APL_TJT",                "b28");
        mapCampos.put("JNL_COD_RTO_HOS",            "b29");
        mapCampos.put("JNL_COD_RTO_ON2",            "b30");
        mapCampos.put("JNL_IND_NO_PRC",             "b31");
        mapCampos.put("JNL_NUM_MBR_TJT",            "b32");
        mapCampos.put("JNL_CTA_DES",                "b33");
        mapCampos.put("JNL_COD_PAG_MSL",            "b34");
        mapCampos.put("JNL_DSP",                    "b35");
        mapCampos.put("JNL_UBI_TNL",                "b36");
        mapCampos.put("JNL_TRN_CPR_EXJ",            "b37");

        return mapCampos;

    }

    static  Map<String,RubrosComercios> readRubrosFromCSV(String fileName){
        Map<String, RubrosComercios> rubros = new HashMap<String, RubrosComercios>();
        Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        // using try with resource, Java 7 feature to close resources
        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {

            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
            while (line != null) {

                // use string.split to load a string array with the values from
                // each line of
                // the file, using a comma as the delimiter
                //System.out.println(line);
                String[] attributes = line.split("\t");
                RubrosComercios rubro = createRubro(attributes);

                // adding book into ArrayList
                rubros.put(attributes[0], rubro);

                // read next line before looping
                // if end of file reached, line would be null
                line = br.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }



        return rubros;

    }

    static RubrosComercios createRubro(String[] metadata){
        String codigo_comercio = 1 <= metadata.length ? metadata[0] : "";
        String cod_pv = 2 <= metadata.length ? metadata[1] : "";
        String rut = 3 <= metadata.length ? metadata [2] : "";
        String nombre_fantasia = 4 <= metadata.length ? metadata[3] : "";
        String glosa_comercio = 5 <= metadata.length ? metadata [4] : "";
        String glosa_rut = 6 <= metadata.length ? metadata[5] : "";
        String rubro_nivel1 = 7 <= metadata.length ? metadata[6] : "";
        String rubro_nivel2 = 8 <= metadata.length ? metadata[7] : "";
        String rubro_nivel3 = 9 <= metadata.length ? metadata[8] : "";
        String id_rubro_nivel1 = 10 <= metadata.length ? metadata[9] : "";
        String id_rubro_nivel2 = 11 <= metadata.length ? metadata[10] : "";
        String id_rubro_nivel3 = 12 <= metadata.length ? metadata[11] : "";
        String subrubro_nivel2 = 13 <= metadata.length ? metadata[12] : "";
        String lat = 14 <= metadata.length ? metadata[13] : "";
        String lon = 15 <= metadata.length ? metadata[14] :"";
        String ind_fuente = 16 <= metadata.length ? metadata[15]: "";
        String minima_fecha = 17 <= metadata.length ? metadata[16]: "";
        String maxima_fecha = 18 <= metadata.length ? metadata[17] : "";
        String comuna = 19 <= metadata.length ? metadata[18]: "";

        return new RubrosComercios(codigo_comercio, cod_pv, rut, nombre_fantasia, glosa_comercio, glosa_rut ,rubro_nivel1, rubro_nivel2, rubro_nivel3, id_rubro_nivel1,
                id_rubro_nivel2, id_rubro_nivel3 ,subrubro_nivel2, lat,  lon,  ind_fuente,  minima_fecha,  maxima_fecha, comuna);
    }

}
