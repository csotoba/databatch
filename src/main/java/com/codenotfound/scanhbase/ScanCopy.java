package com.codenotfound.scanhbase;


import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.ArrayList; // import the ArrayList class
import java.util.List; // import the ArrayList class
import java.util.Arrays; // import the ArrayList class

import static java.lang.Math.abs;

import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;


import org.apache.hadoop.hbase.filter. Filter;
public class ScanCopy {

    public static void main(String args[]) throws IOException {
        List<String> asdf = new ArrayList<String>(Arrays.asList("50003433","10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660"));
        for(String cta : asdf) {
            Configuration config = HBaseConfiguration.create();
            //config.set("hbase.zookeeper.quorum", "10.248.22.9,10.248.22.7,10.248.22.8");
            config.set("hbase.zookeeper.quorum", "10.248.16.5,10.248.16.6,10.248.16.8");
            config.set("hbase.zookeeper.property.clientPort", "2181");
            config.set("hadoop.security.authentication", "kerberos");
            config.set("hbase.rpc.timeout", Integer.toString(60000));
            config.set("hbase.rpc.protection", "privacy");
            config.set("hbase.security.authentication", "kerberos");
            config.set("hbase.cluster.distributed", "true");
            config.set("hbase.master.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
            config.set("hbase.master.keytab.file", "csotoba.keytab");
            config.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
            config.set("hbase.regionserver.keytab.file", "csotoba.keytab");
            config.set("hbase.client.retries.number", Integer.toString(1));
            config.set("zookeeper.session.timeout", Integer.toString(60000));
            config.set("zookeeper.recovery.retry", Integer.toString(10));
            UserGroupInformation.setConfiguration(config);

            // Instanciar la Tabla

            Connection conn = ConnectionFactory.createConnection(config);
            Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_cert"));
            String cta_ori = "0000" + cta;
            cta_ori = DigestUtils.sha1Hex(cta_ori);//.toUpperCase();

            // Instanciar el Scan con filtros correspondientes:

            String cta_des = "0000" + cta; //10003433";
            cta_des = DigestUtils.sha1Hex(cta_des).toUpperCase();

            Scan scan = new Scan();
            Filter prefixFilter = new PrefixFilter(Bytes.toBytes(cta_ori));
            Filter mnmFilter = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("JNL_MNM"), CompareOp.EQUAL, new SubstringComparator("8A"));
            FilterList filterList = new FilterList();
            filterList.addFilter(prefixFilter);
            filterList.addFilter(mnmFilter);

            scan.setFilter(filterList);


            ResultScanner scanner = table_des.getScanner(scan);
            for (Result result = scanner.next(); result != null; result = scanner.next()) {


                // copio JNL TAL Y COMO ESTÁ

                byte[] syskey = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("SYSKEY"));
                long mod = 9223372036854775807L - Long.parseLong(Bytes.toString(syskey));
                String row_des = cta_des + "|" + mod;
                System.out.println(row_des);

                Put put = new Put(Bytes.toBytes(row_des));

                NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap = result.getNoVersionMap();
                for (byte[] familyBytes : familyQualifierMap.keySet()) {
                    NavigableMap<byte[], byte[]> qualifierMap = familyQualifierMap.get(familyBytes);

                    for (byte[] qualifier : qualifierMap.keySet()) {
                        put.addColumn(familyBytes, qualifier, qualifierMap.get(qualifier));
                    }

                }


                Put put_n = new Put(Bytes.toBytes(row_des));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("p5"), Bytes.toBytes(cta));
                String row_des_2 = cta_ori + "|" + mod;
                Put put_n_2 = new Put(Bytes.toBytes(row_des_2));
                put_n_2.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("p5"), Bytes.toBytes(cta));
                table_des.put(put);
                table_des.put(put_n);
                table_des.put(put_n_2);
            }


        }
    }
}
