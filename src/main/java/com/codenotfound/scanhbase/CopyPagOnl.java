package com.codenotfound.scanhbase;
import java.util.*;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static java.lang.Math.abs;

import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;


import org.apache.hadoop.hbase.filter. Filter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
public class CopyPagOnl {

    public static void main(String args[]) throws IOException {
// Configuración para conexión a DataLake

        Configuration config = HBaseConfiguration.create();
        //config.set("hbase.zookeeper.quorum", "10.248.22.9,10.248.22.7,10.248.22.8");
        config.set("hbase.zookeeper.quorum", "10.248.16.5,10.248.16.6,10.248.16.8");
        config.set("hbase.zookeeper.property.clientPort", "2181");
        config.set("hadoop.security.authentication", "kerberos");
        config.set("hbase.rpc.timeout", Integer.toString(60000));
        config.set("hbase.rpc.protection", "privacy");
        config.set("hbase.security.authentication", "kerberos");
        config.set("hbase.cluster.distributed", "true");
        config.set("hbase.master.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.master.keytab.file", "csotoba.keytab");
        config.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.regionserver.keytab.file", "csotoba.keytab");
        config.set("hbase.client.retries.number", Integer.toString(1));
        config.set("zookeeper.session.timeout", Integer.toString(60000));
        config.set("zookeeper.recovery.retry", Integer.toString(10));
        UserGroupInformation.setConfiguration(config);


        Map<String,String> mapaCampos = generaMapaCampos();
        Map<String,String> mapaCNV = generaMapaCNV();
// Instanciar la Tabla

        Connection conn = ConnectionFactory.createConnection(config);
        Table table_JNL = conn.getTable(TableName.valueOf("TDM_CCTOT001:TBJNL01"));
        Table table_mov = conn.getTable(TableName.valueOf("syb_dbo:mov"));
        Table table_cnv = conn.getTable(TableName.valueOf("syb_dbo:cnv"));
        // /Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:prueba_csoto"));
        Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_dev"));

//        List<String> cuentas = new ArrayList<String>(Arrays.asList("13058178", "13058180", "90001792", "50009792", "10005603", "50005603", "49000082"));
        List<String> cuentas = new ArrayList<String>(Arrays.asList("13068521", "13068524", "10002908","50002908","10000830"));

        Long t1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-17 12:30:00", new ParsePosition(0)).getTime();
        Long t2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-17 12:45:00", new ParsePosition(0)).getTime();
        for(String cta_it : cuentas) {

            t1 = t1 - 900000;
            t2 = t2 - 900000;
            String cta_des = "0000" + cta_it; //13058178";
            cta_des = DigestUtils.sha1Hex(cta_des).toUpperCase();
            Scan scan = new Scan();

            scan.setTimeRange(t1, t2);
            Filter mnmFilter = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("JNL_MNM"), CompareOp.EQUAL, new SubstringComparator("8A"));
            //scan.setRowPrefixFilter(cta_ori.getBytes() );
            scan.setFilter(mnmFilter);
            //scan.setCaching(100);
            //scan.setSmall(true);
// Scanear la tabla

            ResultScanner scanner = table_JNL.getScanner(scan);


            int n = 0;
            int N = 10;


            for (Result result = scanner.next(); result != null && n < N; result = scanner.next()) {
                //System.out.println("Hbase: " + Bytes.toString(result.getRow()));
                //String row1 = Bytes.toString(result.getRow());
                //String row2 = cta_des + row1.split("|")[1];
                //String row2 = cta_des + row1.substring(row1.length() - 43);
                byte[] syskey = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("SYSKEY"));
                long mod = 9223372036854775807L - Long.parseLong(Bytes.toString(syskey));
                String row_des = cta_des + "|" + mod;
                System.out.println(row_des);
                Put put = new Put(Bytes.toBytes(row_des));

                // copio JNL TAL Y COMO ESTÁ
                NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap = result.getNoVersionMap();
                for (byte[] familyBytes : familyQualifierMap.keySet()) {
                    NavigableMap<byte[], byte[]> qualifierMap = familyQualifierMap.get(familyBytes);

                    for (byte[] qualifier : qualifierMap.keySet()) {
                        put.addColumn(familyBytes, qualifier, qualifierMap.get(qualifier));
                    }
                }


                //table_des.put(put);
                // ahora busco en la mov
                Scan scan_mov = new Scan();

                byte[] cuenta = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_COP"));
                byte[] mto = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("JNL_CPO_4"));

                t2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-17 17:00:00", new ParsePosition(0)).getTime();

                scan_mov.setTimeRange(t1, t2);
                Filter filterCta = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("mov_cta_car"), CompareOp.EQUAL, new SubstringComparator(Bytes.toString(cuenta).substring(4)));
                Filter filterMto = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("mov_mto"), CompareOp.EQUAL, new SubstringComparator(Bytes.toString(mto)));

                FilterList filterList = new FilterList();
                filterList.addFilter(filterCta);
                filterList.addFilter(filterMto);

                scan_mov.setFilter(filterList);
                ResultScanner scanner_mov = table_mov.getScanner(scan_mov);
                int n1 = 0;
                int N1 = 1;
                for (Result result_mov = scanner_mov.next(); result_mov != null && n1 < N1; result_mov = scanner_mov.next()) {
                    System.out.println("mov" + n);
                    Put put_mov = new Put(Bytes.toBytes(row_des));

                    // copio mov TAL Y COMO ESTÁ
                    int p = 1;
                    NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap_mov = result_mov.getNoVersionMap();
                    for (byte[] familyBytes : familyQualifierMap_mov.keySet()) {
                        NavigableMap<byte[], byte[]> qualifierMap_mov = familyQualifierMap_mov.get(familyBytes);

                        for (byte[] qualifier : qualifierMap_mov.keySet()) {
                            if (mapaCampos.containsKey(Bytes.toString(qualifier))) {
                                String column = mapaCampos.get(Bytes.toString(qualifier));
                                System.out.println(qualifier);
                                System.out.println(Bytes.toString(qualifier));
                                put_mov.addColumn(familyBytes, Bytes.toBytes(column), qualifierMap_mov.get(qualifier));
                            }
                        }
                    }
                    //table_des.put(put_mov);

                    // ahora la cnv
                    Scan scan_cnv = new Scan();
                    byte[] idn = result_mov.getValue(Bytes.toBytes("cf"), Bytes.toBytes("cnv_idn"));
                    Filter filterIdn = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("cnv_idn"), CompareOp.EQUAL, new SubstringComparator(Bytes.toString(idn)));
                    scan_cnv.setFilter(filterIdn);
                    ResultScanner scanner_cnv = table_cnv.getScanner(scan_cnv);

                    int n2 = 0;
                    int N2 = 1;
                    for (Result result_cnv = scanner_cnv.next(); result_cnv != null && n2 < N2; result_cnv = scanner_cnv.next()) {
                        Put put_cnv = new Put(Bytes.toBytes(row_des));
                        NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap_cnv = result_cnv.getNoVersionMap();
                        for (byte[] familyBytes : familyQualifierMap_cnv.keySet()) {
                            NavigableMap<byte[], byte[]> qualifierMap_cnv = familyQualifierMap_cnv.get(familyBytes);

                            for (byte[] qualifier : qualifierMap_cnv.keySet()) {
                                if (mapaCNV.containsKey(Bytes.toString(qualifier))) {
                                    String column_cnv = mapaCNV.get(Bytes.toString(qualifier));
                                    put_cnv.addColumn(familyBytes, Bytes.toBytes(column_cnv), qualifierMap_cnv.get(qualifier));
                                }
                            }
                        }
                        table_des.put(put_cnv);
                        table_des.put(put_mov);
                        table_des.put(put);
                        n2++;
                        Put put_n = new Put(Bytes.toBytes(row_des));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n1"), Bytes.toBytes("8A"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n2"), Bytes.toBytes("C"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n3"), Bytes.toBytes("Pago cuentas via Internet"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n4"), Bytes.toBytes("879"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n5"), Bytes.toBytes("Pago Cuentas Via Internet"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n6"), Bytes.toBytes("693"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n7"), Bytes.toBytes("Pago Cuentas Via Internet"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n8"), Bytes.toBytes("524"));
                        put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n9"), Bytes.toBytes("Pagos en línea"));
                        table_des.put(put_n);
                        Put put_n_2 = new Put(Bytes.toBytes(row_des));
                        put_n_2.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("p5"), Bytes.toBytes(cta_it));
                        table_des.put(put_n_2);


                    }


                    n1++;
                    n++;
                    System.out.println("rec: " + n);
                }


                //System.out.println(result);



            }
            scanner.close();
        }
//Cerrar la tabla


        table_JNL.close();
        table_des.close();

    }

    static Map<String,String> generaMapaCampos() {
        Map<String, String> mapCampos = new HashMap<String, String>();
        //TRANSFERENCIAS TRA
        mapCampos.put("tra_cli_idc",      "t1");
        mapCampos.put("tra_cmt",          "t2");
        mapCampos.put("tra_cnl",          "t3");
        mapCampos.put("tra_cod_bco_dst",  "t4");
        mapCampos.put("tra_cod_err",      "t5");
        mapCampos.put("tra_cod_est",      "t6");
        mapCampos.put("tra_cta_dst",      "t7");
        mapCampos.put("tra_cta_ori",      "t8");
        mapCampos.put("tra_dir_cor_dst",  "t9");
        mapCampos.put("tra_dir_cor_ori",  "t10");
        mapCampos.put("tra_fec_abo",      "t11");
        mapCampos.put("tra_fec_car",      "t12");
        mapCampos.put("tra_fec_cmb_est",  "t13");
        mapCampos.put("tra_fec_cre",      "t14");
        mapCampos.put("tra_fol_sip",      "t15");
        mapCampos.put("tra_gls_err",      "t16");
        mapCampos.put("tra_id_ali",       "t17");
        mapCampos.put("tra_mto_trf",      "t18");
        mapCampos.put("tra_nom_bnf",      "t19");
        mapCampos.put("tra_num_ope",      "t20");
        mapCampos.put("tra_nun_ser_sip",  "t21");
        mapCampos.put("tra_rut_cta_dst",  "t22");
        mapCampos.put("tra_tip_cta_dst",  "t23");
        mapCampos.put("tra_tip_cta_ori",  "t24");
        mapCampos.put("tra_tip_trf",      "t25");
        mapCampos.put("tra_usu_act",      "t26");
        // MOV Y CNV
        mapCampos.put("mov_cod_err",      "p1");
        mapCampos.put("mov_cod_fct",      "p2");
        mapCampos.put("mov_cof_rto",      "p3");
        mapCampos.put("mov_cod_ser",      "p4");
        mapCampos.put("mov_cta_car",      "p5");
        mapCampos.put("mov_dig_vrf",      "p6");
        mapCampos.put("mov_dst_car",      "p7");
        mapCampos.put("mov_est_cua",      "p8");
        mapCampos.put("mov_fec_ctb",      "p9");
        mapCampos.put("mov_fec_est_cua",  "p10");
        mapCampos.put("mov_fec_pag",      "p11");
        mapCampos.put("mov_idn_ser",      "p12");
        mapCampos.put("mov_mto",          "p13");
        mapCampos.put("mov_srl",          "p14");
        mapCampos.put("mov_trx",          "p15");
        mapCampos.put("mov_idc",          "p16");
        mapCampos.put("cnv_idn",          "p17");
        return mapCampos;

    }

    static Map<String,String> generaMapaCNV() {
        Map<String, String> mapCNV = new HashMap<String, String>();
        mapCNV.put("cnv_clave",        "p23");
        mapCNV.put("cnv_cms_cct",      "p24");
        mapCNV.put("cnv_cms_tdc",      "p25");
        mapCNV.put("cnv_cta_cte",      "p26");
        mapCNV.put("cnv_des_cli",      "p27");
        mapCNV.put("cnv_est",          "p28");
        mapCNV.put("cnv_fec_exp",      "p29");
        mapCNV.put("cnv_fec_ini",      "p30");
        mapCNV.put("cnv_flg_cms",      "p31");
        mapCNV.put("cnv_flg_seg",      "p32");
        mapCNV.put("cnv_gls",          "p33");
        mapCNV.put("cnv_help",         "p34");
        mapCNV.put("cnv_idn",          "p35");
        mapCNV.put("cnv_idn_ori",      "p36");
        mapCNV.put("cnv_img",          "p37");
        mapCNV.put("cnv_ind_pop",      "p38");
        mapCNV.put("cnv_len_srv",      "p39");
        mapCNV.put("cnv_msk_ip",       "p40");
        mapCNV.put("cnv_mto_max_trx",  "p41");
        mapCNV.put("cnv_opc_rdc_cda",  "p42");
        mapCNV.put("cnv_opc_rdc_cpr",  "p43");
        mapCNV.put("cnv_preingreso",   "p44");
        mapCNV.put("cnv_protocolo",    "p45");
        mapCNV.put("cnv_rdc_cda_anx",  "p46");
        mapCNV.put("cnv_rdc_cpr_anx",  "p47");
        mapCNV.put("cnv_tip",          "p48");
        mapCNV.put("cnv_tip_rnd",      "p49");
        mapCNV.put("cnv_url",          "p50");
        mapCNV.put("cnv_url_endpoint", "p51");
        mapCNV.put("cnv_usuario",      "p52");
        mapCNV.put("mnd_idn",          "p53");

        return mapCNV;
    }

}
