package com.codenotfound.scanhbase;

public class RubrosComercios {
    private String codigo_comercio;
    private String cod_pv;
    private String rut;
    private String nombre_fantasia;
    private String glosa_comercio;
    private String glosa_rut;
    private String rubro_nivel1;
    private String rubro_nivel2;
    private String rubro_nivel3;
    private String id_rubro_nivel1;
    private String id_rubro_nivel2;
    private String id_rubro_nivel3;
    private String subrubro_nivel2;
    private String lat;
    private String lon;
    private String ind_fuente;
    private String minima_fecha;
    private String maxima_fecha;
    private String comuna;

    public RubrosComercios(String codigo_comercio, String cod_pv, String rut, String nombre_fantasia, String glosa_comercio, String glosa_rut
    ,String rubro_nivel1, String rubro_nivel2, String rubro_nivel3, String id_rubro_nivel1, String id_rubro_nivel2, String id_rubro_nivel3
    ,String subrubro_nivel2, String lat, String lon, String ind_fuente, String minima_fecha, String maxima_fecha, String comuna){
        this.codigo_comercio = codigo_comercio;
        this.cod_pv = cod_pv;
        this.rut = rut;
        this.nombre_fantasia = nombre_fantasia;
        this.glosa_comercio = glosa_comercio;
        this.glosa_rut = glosa_rut;
        this.rubro_nivel1 = rubro_nivel1;
        this.rubro_nivel2 = rubro_nivel2;
        this.rubro_nivel3 = rubro_nivel3;
        this.id_rubro_nivel1 = id_rubro_nivel1;
        this.id_rubro_nivel2 = id_rubro_nivel2;
        this.id_rubro_nivel3 = id_rubro_nivel3;
        this.subrubro_nivel2 = subrubro_nivel2;
        this.lat = lat;
        this.lon = lon;
        this.ind_fuente = ind_fuente;
        this.minima_fecha = minima_fecha;
        this.maxima_fecha = maxima_fecha;
        this.comuna = comuna;
    }

    public String getCodigo_comercio() {
        return codigo_comercio;
    }

    public void setCodigo_comercio(String codigo_comercio) {
        this.codigo_comercio = codigo_comercio;
    }

    public String getCod_pv() {
        return cod_pv;
    }

    public void setCod_pv(String cod_pv) {
        this.cod_pv = cod_pv;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre_fantasia() {
        return nombre_fantasia;
    }

    public void setNombre_fantasia(String nombre_fantasia) {
        this.nombre_fantasia = nombre_fantasia;
    }

    public String getGlosa_comercio() {
        return glosa_comercio;
    }

    public void setGlosa_comercio(String glosa_comercio) {
        this.glosa_comercio = glosa_comercio;
    }

    public String getGlosa_rut() {
        return glosa_rut;
    }

    public void setGlosa_rut(String glosa_rut) {
        this.glosa_rut = glosa_rut;
    }

    public String getRubro_nivel1() {
        return rubro_nivel1;
    }

    public void setRubro_nivel1(String rubro_nivel1) {
        this.rubro_nivel1 = rubro_nivel1;
    }

    public String getRubro_nivel2() {
        return rubro_nivel2;
    }

    public void setRubro_nivel2(String rubro_nivel2) {
        this.rubro_nivel2 = rubro_nivel2;
    }

    public String getRubro_nivel3() {
        return rubro_nivel3;
    }

    public void setRubro_nivel3(String rubro_nivel3) {
        this.rubro_nivel3 = rubro_nivel3;
    }

    public String getId_rubro_nivel1() {
        return id_rubro_nivel1;
    }

    public void setId_rubro_nivel1(String id_rubro_nivel1) {
        this.id_rubro_nivel1 = id_rubro_nivel1;
    }

    public String getId_rubro_nivel2() {
        return id_rubro_nivel2;
    }

    public void setId_rubro_nivel2(String id_rubro_nivel2) {
        this.id_rubro_nivel2 = id_rubro_nivel2;
    }

    public String getId_rubro_nivel3() {
        return id_rubro_nivel3;
    }

    public void setId_rubro_nivel3(String id_rubro_nivel3) {
        this.id_rubro_nivel3 = id_rubro_nivel3;
    }

    public String getSubrubro_nivel2() {
        return subrubro_nivel2;
    }

    public void setSubrubro_nivel2(String subrubro_nivel2) {
        this.subrubro_nivel2 = subrubro_nivel2;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getInd_fuente() {
        return ind_fuente;
    }

    public void setInd_fuente(String ind_fuente) {
        this.ind_fuente = ind_fuente;
    }

    public String getMinima_fecha() {
        return minima_fecha;
    }

    public void setMinima_fecha(String minima_fecha) {
        this.minima_fecha = minima_fecha;
    }

    public String getMaxima_fecha() {
        return maxima_fecha;
    }

    public void setMaxima_fecha(String maxima_fecha) {
        this.maxima_fecha = maxima_fecha;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }
}
