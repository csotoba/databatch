package com.codenotfound.scanhbase;
import java.util.*;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static java.lang.Math.abs;

import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;


import org.apache.hadoop.hbase.filter. Filter;
import org.apache.hadoop.hbase.filter.SubstringComparator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
public class CopyCredito {

    public static void main(String args[]) throws IOException {
// Configuración para conexión a DataLake
        Configuration config = HBaseConfiguration.create();
        //config.set("hbase.zookeeper.quorum", "10.248.22.9,10.248.22.7,10.248.22.8");
        config.set("hbase.zookeeper.quorum", "10.248.16.5,10.248.16.6,10.248.16.8");
        config.set("hbase.zookeeper.property.clientPort", "2181");
        config.set("hadoop.security.authentication", "kerberos");
        config.set("hbase.rpc.timeout", Integer.toString(60000));
        config.set("hbase.rpc.protection", "privacy");
        config.set("hbase.security.authentication", "kerberos");
        config.set("hbase.cluster.distributed", "true");
        config.set("hbase.master.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.master.keytab.file", "csotoba.keytab");
        config.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@BCIDATALAKE.CL");
        config.set("hbase.regionserver.keytab.file", "csotoba.keytab");
        config.set("hbase.client.retries.number", Integer.toString(1));
        config.set("zookeeper.session.timeout", Integer.toString(60000));
        config.set("zookeeper.recovery.retry", Integer.toString(10));
        UserGroupInformation.setConfiguration(config);



// Instanciar la Tabla

        Connection conn = ConnectionFactory.createConnection(config);
        Table table_JNL = conn.getTable(TableName.valueOf("TDM_CCTOT001:TBJNL01"));
        //Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:prueba_csoto"));
        Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_cert"));
        //Table table_des = conn.getTable(TableName.valueOf("db_hba_analytics:mov_enr_v2_dev"));

        // "10003262","50003262","10003431","50003431","10003433","50003433","10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660"
        List<String> cuentas = new ArrayList<String>(Arrays.asList("10003262","50003262","10003431","50003431","10003433","50003433","10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660"));
        //List<String> cuentas = new ArrayList<String>(Arrays.asList("10003307","50003307","34333502","91991723","70495132","70995132","60048660","99048660","13058178", "13058180", "90001792", "50009792", "10005603", "50005603", "49000082"));
        //List<String> cuentas = new ArrayList<String>(Arrays.asList("13068521", "13068524", "10002908","50002908","10000830"));
        //List<String> cuentas = new ArrayList<String>(Arrays.asList("13068521"));


        Long t1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-21 16:20:00", new ParsePosition(0)).getTime();
        Long t2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-09-21 16:30:00", new ParsePosition(0)).getTime();
        for(String cta_it : cuentas) {


            String cta_des = "0000" + cta_it; //13058178";
            cta_des = DigestUtils.sha1Hex(cta_des).toUpperCase();

            Scan scan = new Scan();
            t1 = t1 - 600000;
            t2 = t2 - 600000;
            scan.setTimeRange(t1, t2);
            Filter mnmFilter = new SingleColumnValueFilter(Bytes.toBytes("cf"), Bytes.toBytes("JNL_MNM"), CompareOp.EQUAL, new SubstringComparator("1Y"));
            scan.setFilter(mnmFilter);


            ResultScanner scanner = table_JNL.getScanner(scan);

            int N = 10;
            int counter = 0;

            for (Result result = scanner.next(); result != null && counter < N; result = scanner.next()) {
                byte[] syskey = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("SYSKEY"));
                long mod = 9223372036854775807L - Long.parseLong(Bytes.toString(syskey));
                String row_des = cta_des + "|" + mod;
                System.out.println(row_des);
                Put put = new Put(Bytes.toBytes(row_des));

                // copio JNL TAL Y COMO ESTÁ
                NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap = result.getNoVersionMap();
                for (byte[] familyBytes : familyQualifierMap.keySet()) {
                    NavigableMap<byte[], byte[]> qualifierMap = familyQualifierMap.get(familyBytes);

                    for (byte[] qualifier : qualifierMap.keySet()) {
                        put.addColumn(familyBytes, qualifier, qualifierMap.get(qualifier));
                    }
                }

                table_des.put(put);
                Put put_n = new Put(Bytes.toBytes(row_des));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n1"), Bytes.toBytes("1Y"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n2"), Bytes.toBytes("C"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n3"), Bytes.toBytes("Pago deuda Tarjeta de Crédito"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n4"), Bytes.toBytes("865"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n5"), Bytes.toBytes("Pago deuda Tarjeta de Crédito"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n6"), Bytes.toBytes("679"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n7"), Bytes.toBytes("Pago deuda Tarjeta de Crédito"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n8"), Bytes.toBytes("522"));
                put_n.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("n9"), Bytes.toBytes("Pago de la tarjeta de crédito"));
                table_des.put(put_n);

                counter++;
                System.out.println("regs: " + counter);

            }

            scanner.close();

        }
//Cerrar la tabla
            table_JNL.close();
            table_des.close();


    }

}
