package com.codenotfound.scanhbase;

public class ATM {
    private String numero;
    private String ind_atm;
    private String region;
    private String direccion;
    private String comuna;
    private String ciudad;
    private String sub_local;

    public ATM(String numero, String ind_atm, String region, String direccion, String comuna, String ciudad, String sub_local) {
        this.numero = numero;
        this.ind_atm = ind_atm;
        this.region = region;
        this.direccion = direccion;
        this.comuna = comuna;
        this.ciudad = ciudad;
        this.sub_local = sub_local;
    }

    public String getNumero() {
        return numero;
    }

    public String getInd_atm() {
        return ind_atm;
    }

    public String getRegion() {
        return region;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getSub_local() {
        return sub_local;
    }
}

